import SelectedLanguage from "./SelectedLanguage";
import Repos from "./Repos";

const Popular = () => {
  return (
    <div>
        <>
          <SelectedLanguage />
          <Repos />
        </>
    </div>
  );
};

export default Popular;


