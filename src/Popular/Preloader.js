const Preloader = () => {
  return (
    <div id="preloader" className="preloader">
      <div className="preloader__loader"></div>
      <div className="loadingio-spinner-rolling-9yjmcrpk6y">
        <div className="ldio-r8owy0206n">
          <div></div>
        </div>
      </div>
    </div>
  );
};

export default Preloader;
