import { battle } from "../../api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { setLoading, setFirstPlayer, setSecondPlayer, setFirstPlayerScore, setSecondPlayerScore, setError } from "./battle.slice";

export const fetchBattleData = createAsyncThunk(
  'battle/fetchBattleData',
  async (params, {dispatch}) => {
    try {
      dispatch(setLoading(true));
      const [playerOne, playerTwo] = await battle([params.get('playerOneName'), params.get('playerTwoName')]);
      dispatch(setFirstPlayer(playerOne.profile));
      dispatch(setSecondPlayer(playerTwo.profile));
      dispatch(setFirstPlayerScore(playerOne.score));
      dispatch(setSecondPlayerScore(playerTwo.score));
      dispatch(setLoading(false));
    } catch (error) {
      dispatch(setError(error));
      dispatch(setLoading(false));
    }
  }
);