import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import { setSelectedLanguage } from "../redux/popular/popular.slice";
import { fetchPopularRepos } from "../redux/popular/popular.request";

const languages = ['All', 'Javascript', 'Ruby', 'Java', 'CSS', 'Python'];

const SelectedLanguage = () => {
    const dispatch = useDispatch();
    const selectedLanguage = useSelector(state => state.popular.selectedLanguage);

    useEffect(() => {
        dispatch(fetchPopularRepos(selectedLanguage));
    }, []);

    return (
        <ul className='languages'>
            {languages.map((language, index) => {
                return (
                    <li
                        key={index}
                        style={{color: language === selectedLanguage ? '#d0021b' : '#000000'}}
                        onClick={() => {
                            dispatch(setSelectedLanguage(language))
                            dispatch(fetchPopularRepos(language));
                        }}
                    >
                        {language}
                    </li>
                )
            })}
        </ul>
    )
}

export default SelectedLanguage;