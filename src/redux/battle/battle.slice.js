import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    playerOneName: '',
    playerTwoName: '',
    playerOneImage: null,
    playerTwoImage: null,
    firstPlayer: {},
    secondPlayer: {},
    firstPlayerScore: null,
    secondPlayerScore: null,
    error: null,
    loading: false,
}

const battleSlice = createSlice({
  name: 'battle',
  initialState,
  reducers: {
    setFirstPlayer: (state, {payload}) => {
        state.firstPlayer = payload;
    },
    setSecondPlayer: (state, {payload}) => {
        state.secondPlayer = payload;
    },
    setFirstPlayerScore: (state) => {
        state.firstPlayerScore = null;
    },
    setSecondPlayerScore: (state) => {
        state.secondPlayerScore = null;
    },
    setError: (state, { payload }) => {
        state.error = payload;
    },
    setLoading: (state, { payload }) => {
        state.loading = payload;
    },
    setPlayerName: (state, { payload }) => {
        const { id, username } = payload;
        state[`${id}Name`] = username;
    },
    setPlayerImage: (state, { payload }) => {
        const { id, image } = payload;
        state[`${id}Image`] = image;
    },
    resetPlayer: (state, { payload }) => {
        const { id } = payload;
        state[`${id}Name`] = '';
        state[`${id}Image`] = null;
    },
    },
  });

const {actions, reducer} = battleSlice;

export const {
    setFirstPlayer,
    setSecondPlayer,
    setFirstPlayerScore,
    setSecondPlayerScore,
    setError,
    setLoading,
    setPlayerName,
    setPlayerImage,
    resetPlayer,
} = actions;
  
export default reducer;